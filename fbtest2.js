(function(){

function TreeNode(value){
    this.value = value;
    this.left;
    this.right;
}

function calcAverage(node){
    if(!node){ return; }

    var levelSums = []; var adjacents = []
    adjacents.push(node)

    while(adjacents.length > 0){
        var nextNodes = []; var sum = 0
        for(var i=0; i < adjacents.length; i++){
            var vnode = adjacents[i]
            if(vnode){
                sum = sum + parseInt(vnode.value);
                if(vnode.left){ nextNodes.push(vnode.left) }
                if(vnode.right){ nextNodes.push(vnode.right) }
            }
        }
        levelSums.push(Math.round(sum / adjacents.length));
        adjacents = nextNodes;
    }

    return levelSums;

}

var node = new TreeNode(4)
node.left = new TreeNode(7)
node.right = new TreeNode(9)

node.left.left = new TreeNode(10)
node.left.right = new TreeNode(2)
node.left.right.right =  new TreeNode(6)
node.left.right.right.left =  new TreeNode(2)

node.right.right = new TreeNode(6)


var res = calcAverage(node);

console.log(res)

})()