// Add any extra import statements you may need here


// Add any helper functions you may need here
/*
int arr n ? queue
ordered front to back ? asc
x iterations - 3 steps
  - remove x items, if less remove all from front 
  - find largest discard (if multiple discard earliest)
  - decrement value by 1 if val > 0 
  - add back to end of queue
  
  item Object
    - value
    - index
  
  Queue Object
    - items
    - poppedItemIndex
    + add
    + remove
    + findMax
    + run (3, step)
  
*/

function Item(value, index){
    this.value = value;
    this.index = index;
  }
  
  function Queue(ct){
    this.maxPopCt = ct;
    this.items = [];
    this.poppedItemIndices = [];
    
    //to initialize list
    this.add = function(val){
      var item = new Item(val, (this.items.length + 1) )
      this.items.push(item)
    };
    
    this.pushItems = function(item){
       this.items.push(item)
    }
    
    this.removeItems = function(){
      var removedItems = [];
      var ct = (this.maxPopCt <= this.items.length) ? this.maxPopCt : this.items.length
      for(var i=0; i < ct; i++){
          var item = this.items.shift()
          if(item){
            removedItems.push( item )
          }
       
      }
      return removedItems;
    };
    
    //[Item, Item..]
    this.findMax = function(arr){
      
      var max = 0, maxIds = [], minId=0;
      for(var i=0; i < arr.length; i++){
        if(arr[i].value > max){
          max = arr[i].value; 
          if(maxIds.length == 0){ 
              maxIds.push(i) 
            }else{ 
              maxIds = [i]
            }
        }else if(arr[i].value == max){
            maxIds.push(i);
        }
        
      }

      //[2,3]
      if(maxIds.length == 1 ){
        return maxIds[0]
      }else {

        var min=0, minId=-1;
        for(var i=0; i < maxIds.length; i++){
          var item = arr[maxIds[i]];
          if(minId == -1){
            min = item.index
            minId = maxIds[i]
          }else
          if(item.index < min){
            min = item.index
            minId = maxIds[i]
          }
        }

        return minId
      }
  
    };
    
    this.run = function(){
 
      var removedItems = this.removeItems()
      var maxIndex = this.findMax(removedItems)

      if((maxIndex > -1) && (maxIndex < removedItems.length)){
        var maxItem = removedItems.splice(maxIndex, 1)[0]

        this.poppedItemIndices.push(maxItem.index)
        
        for(var i=0; i < removedItems.length; i++){
          if(removedItems[i].value >= 1){
            removedItems[i].value = removedItems[i].value - 1
          }
          this.pushItems(removedItems[i])
        }
      }
      
    }
  }
  
  function findPositions(arr, x) {
    // Write your code here
    var App = new Queue(x);
    
    for(var i=0; i < arr.length; i++ ){
      App.add(arr[i]);
    }
    
    for(var i=0; i < x; i++){
      App.run()
    }
    
    return App.poppedItemIndices;
  }
  
  
  
  
  
  
  
  
  
  
  
  // These are the tests we use to determine if the solution is correct.
  // You can add your own at the bottom, but they are otherwise not editable!
  function printintegerArray(array) {
    var size = array.length;
    var res = '';
    res += '[';
    var i = 0;
    for (i = 0; i < size; i++) {
      if (i !== 0) {
          res += ', ';
      }
      res += array[i];
    }
    res += ']';
    return res;
  }
  
  var test_case_number = 1;
  
  function check(expected, output) {
    var expected_size = expected.length;
    var output_size = output.length;
    var result = true;
    if (expected_size != output_size) {
      result = false;
    }
    for (var i = 0; i < Math.min(expected_size, output_size); i++) {
      result &= (output[i] == expected[i]);
    }
    var rightTick = "\u2713";
      var wrongTick = "\u2717";
    if (result) {
        var out = rightTick + ' Test #' + test_case_number;
        console.log(out);
    }
    else {
        var out = '';
        out += wrongTick + ' Test #' + test_case_number + ': Expected ';
        out += printintegerArray(expected);
        out += ' Your output: ';
        out += printintegerArray(output);
        console.log(out);
    }
    test_case_number++;
  }
  
  var n_1 = 6
  var x_1 = 5
  var arr_1 = [1, 2, 2, 3, 4, 5];
  var expected_1 = [5, 6, 4, 1, 2 ];
  var output_1 = findPositions(arr_1, x_1);
  check(expected_1, output_1);
  
  var n_2 = 13
  var x_2 = 4
  var arr_2 = [2, 4, 2, 4, 3, 1, 2, 2, 3, 4, 3, 4, 4];
  var expected_2 = [2, 5, 10, 13];
  var output_2 = findPositions(arr_2, x_2);
  check(expected_2, output_2);
  
  
  // Add your own test cases here
  