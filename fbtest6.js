// Add any extra import statements you may need here

function MaxTuple(){
    this.vals = [0,0];
    
    this.add = function(x){
      if(x > this.vals[1]){
        var b = this.vals[1]
        this.vals[0] = b
        this.vals[1] = x
      }else{
        if(x > this.vals[0]){
            this.vals[0] = x
          }
      }
      
      
    };
    
  }
  
  function findMaxSum(arr){
    
    var max = new MaxTuple()
    for(var i=0; i < arr.length; i++){
      max.add(arr[i])
    }
    
    return max.vals;
  }
  
  // Add any helper functions you may need here
  
  
  function getTotalTime(arr) {
    // Write your code here
    if(!Array.isArray(arr) || arr.length == 0){ return; }
    
    while(arr.length > 1){
        var maxes = findMaxSum(arr);
      
        //remove the largest numbers
        arr.splice( arr.indexOf(maxes[0]), 1)
        arr.splice( arr.indexOf(maxes[1]), 1)
    
        //add the sum of the larges numbers
         arr.push( maxes[0] + maxes[1] )
    }
    
    return arr[0];
  }
  
  
  
  
  
console.log(findMaxSum([4, 2, 1, 3]))

console.log(findMaxSum([ 2, 1, 7]))

console.log(findMaxSum([ 9, 1]))
  
  
  /*
  
  // These are the tests we use to determine if the solution is correct.
  // You can add your own at the bottom, but they are otherwise not editable!
  function printInteger(n) {
    var out = '[' + n + ']';
    return out;
  }
  
  var test_case_number = 1;
  
  function check(expected, output) {
    var result = (expected == output);
    var rightTick = "\u2713";
    var wrongTick = "\u2717";
    if (result) {
      var out = rightTick + ' Test #' + test_case_number;
      console.log(out);
    }
    else {
      var out = '';
      out += wrongTick + ' Test #' + test_case_number + ': Expected ';
      out += printInteger(expected);
      out += ' Your output: ';
      out += printInteger(output);
      console.log(out);
    }
    test_case_number++;
  }
  
  var arr_1 = [4, 2, 1, 3];
  var expected_1 = 26;
  var output_1 = getTotalTime(arr_1);
  check(expected_1, output_1);
  
  var arr_2 = [2, 3, 9, 8, 4];
  var expected_2 = 88;
  var output_2 = getTotalTime(arr_2);
  check(expected_2, output_2);
  
  // Add your own test cases here
  */