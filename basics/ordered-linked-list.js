(()=>{
    const assert = require("assert");

    class Node {
    
        constructor(value, next){
            this.value = value;
            this.next = next || null;
        }
    }  
    
   
    class OrderedLinkedList {
        
        constructor(){
            this.head = null;  //head node
        }

        //insertion sort
        add(node) {
            if (this.head == null) {
                this.head = node;  
            } else {
             
                let current = this.head, previous = null;
                while(current.next !== null){
                    if(current.value >= node.value){
                        break;
                    }
                    previous = current;
                    current = current.next
                }

                //create current replica, essentially breaking link
                let temp = new Node(current.value, current.next)
                node.next = temp //re-attach temp to node, making node positioned infront

                if(previous == null){
                    this.head = node;
                }else{
                    previous.next = node;
                }
                
            }
        };
    
        countNodes () {
            var ct = 0;
            let current = this.head;
            while(current !== null){
                ct++;
                current = current.next;
            }
            return ct;
        };
    
        search(item) {
            let current = this.head;
            while(current !== null){
                if(current.value == item){
                    return true;
                }
                current = current.next;
            }
            return false
        };
    
        remove(value){
            let previous=null, current = this.head;
    
            //traverse list
            while(current !== null  ){
                if(current.value == value){
                    break;
                }
                previous = current;
                current = current.next;
            }
    
            if(previous == null){
                current = previous
            }else{
                previous.next = current.next;
            }
        };
    
    };
    
    
    /**Tests */
    /*
    var myList = new OrderedLinkedList();
    myList.add(new Node(7));
    myList.add(new Node(4));
    myList.add(new Node(1));
    myList.add(new Node(2));
    myList.add(new Node(5));
    myList.add(new Node(3));
   
    assert.strictEqual(myList.countNodes(), 6)
    assert.strictEqual(myList.search(10), false)
    assert.strictEqual(myList.search(5), true)
    myList.remove(2)
    assert.strictEqual(myList.countNodes(), 5)
    assert.strictEqual(myList.search(2), false)
    */
    
    
    module.exports = { Node, OrderedLinkedList };
})()

