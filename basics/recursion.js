(()=>{
    const assert = require("assert");

    const numberSum = (arr) =>{
        let total = 0;
        const addnum = () =>{
            if(arr.length > 0){
                total = total + arr.pop();
                addnum();
            }
        }
        addnum()
        return total;
    }

    const decimalToBase = (num, base) =>{
        // num % base, save remainder
        // math.floor(num / base) track the val
        //repeat until 0
        let DIGITS = '0123456789abcdef'
        let res = []
        const tranform = (val)=>{
            let div = Math.floor(val / base);
            res.unshift(DIGITS[(val % base)])
            if(div > 0){
                tranform(div)
            }
        }
        tranform(num)
        return res.join("")
    }

    let nums = [3,4,5,7,1,2,10]

    assert.strictEqual( numberSum(nums), 32  )
    assert.strictEqual( decimalToBase(42, 2),  "101010")

})()