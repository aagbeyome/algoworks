(() => {
    /**
     * This algorithm checks a given string to determine whether it can be considered
     * balanced based on each opening tag having a corresponding closing tag. e.g.
     * ( () () ), [{()}] is balanced
     * )()()( is not balanced
     * The algorithm is extended to implment a rudimentary JSON string verifier
     */
    const assert = require("assert");
    const Stack = require("./stack.js");

    const isBalanced = (instr) => {
        let res = false;
        const pairs = {
            "(": ")",
            "[": "]",
            "{": "}",
            "\"": "\""
        }

        let stack = new Stack();
        let quoteStack = new Stack();
        let opening = [], closing = [];
        for (var k in pairs) {
            opening.push(k)
            closing.push(pairs[k])
        }

        if (instr.length > 0) {
            for (var i = 0; i < instr.length; i++) {
                let chr = instr[i];

                if (chr == '"') {
                    if (quoteStack.size() < 1) {
                        quoteStack.push(chr)
                    } else {
                        quoteStack.pop()
                    }
                } else {
                    if (opening.indexOf(chr) > -1) {
                        stack.push(chr)
                    } else
                        if (closing.indexOf(chr) > -1) {
                            //peek to see if there is matching opening tag on top of stack
                            let cur = stack.peek()
                            if (cur) {
                                if (pairs[cur] == chr) {
                                    stack.pop()
                                } else {
                                    return false;
                                }
                            } else {
                                return false;
                            }
                        }
                }
            }

            res = (stack.size() == 0 && quoteStack.size() == 0)
        }

        return res;
    }

    assert.strictEqual(isBalanced(""), false);
    assert.strictEqual(isBalanced("123ase#$"), true);
    assert.strictEqual(isBalanced("( () () )"), true);
    assert.strictEqual(isBalanced("( ((())) )"), true);
    assert.strictEqual(isBalanced('[{("")}]'), true);
    assert.strictEqual(isBalanced('[{("")"}]'), false);
    assert.strictEqual(isBalanced(`[(")}]`), false);
    assert.strictEqual(isBalanced(')(())'), false);


})()