(()=>{
    const assert = require("assert");

    function TreeNode(value){
        this.left;
        this.right;
        this.value = value;
    }

    function traverse(node){
        if(node){
           console.log(node.value)
           traverse(node.left)
           traverse(node.right)
        }
    }

    var root = new TreeNode("R10");
    root.left = new TreeNode("RL5");
    root.right = new TreeNode("RR5");
    root.left.left = new TreeNode("RLL3");
    root.left.right = new TreeNode("RLR2");
    root.right.left = new TreeNode("RRL3");
    root.right.right = new TreeNode("RRR2");

    traverse(root)
    
    //console.log(JSON.stringify(root, null, "\t"))

})()