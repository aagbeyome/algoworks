(()=>{
    const assert = require("assert");


    /**
     * [1,3,4,5,7,9,11,13], 11
     * (0 + 7) / 2 = 3
     * (4 + 7) / 2 = 5
     * (6 + 7) / 2 = 6
     */
    const search = (arr, item) =>{
        if(!Array.isArray(arr) && isNaN(item)){
            console.log("Please provide an ordered array of number and a number to search")
            return
        }
        let found = false
        const find = (st, end) =>{
            let mid = Math.floor((st + end) / 2)
            
            if(arr[st] == item || arr[end] == item || arr[mid] == item){ found = true; return; }
            if(arr[mid] > item){ end = mid-1; }
            if(arr[mid] < item){ st = mid+1; }
            
            if(st !== end){
                find(st, end)
            }else{
                found = false; return;
            }
        }

        find(0, arr.length - 1)
        return found;
    }

    /**Tests */
    assert.strictEqual( search( [1,3,4,5,7,9,11,13], 11 ), true)
    assert.strictEqual( search( [1,3,4,5,7,9,11,13], 17 ), false)
    assert.strictEqual( search( [1,3,4,5,7,9,11,13], 1 ), true)
    assert.strictEqual( search( [1,3,4,5,7,9,11,13], 13 ), true)


})()