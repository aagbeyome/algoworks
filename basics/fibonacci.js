(() => {
    const assert = require("assert");

    /**
         * 0,1,1,2,3,5,8 where f(0) = 0, f(1) = 1
         * f(n) = f(n-1) + f(n-2)
         * e.g.
         * f(3) = f(2) + f(1)
         *      = f(1) + f(0) + f(1)
         *      = 1  +  0 + 1
         *      = 2
         */

    // O(2 ^ n) - 2 power n
    const fibonacci = (n) => {
        if (parseInt(n) >= 0) {
            const getFib = (n) => {
                ct++;
                if (n <= 1) { return n }
                return (getFib(n - 1) + getFib(n - 2)) 
            }

            return getFib(n)
            
        } else {
            console.log("provide number >= 0")
        }

    }

    //O(n) but with space complexity due to keeping computed values
    const fibonacciMemo = (n) => {
       
        if (parseInt(n) >= 0) {
            let computedValues = {}

            const getFib = (n) => {
               
                if (computedValues[n]) {
                    return computedValues[n]
                } else {
                    let val = 0;
                    if (n <= 1) {
                        val = n
                    }
                    else {
                        val = (getFib(n - 1) + getFib(n - 2))
                    }
                    computedValues[n] = val; //save result
                    return val;
                }

            }

            return getFib(n)

        } else {
            console.log("provide number >= 0")
        }
    }

    //looping summation method
    //O(n) running time 0(1) space complexity, most performant
    const fibonacciCalc = (n) => {
       
        if(parseInt(n) >= 0){
            let a = 0, b=1, sum=0;
            if(n <= 1){ return n; }
            while(n > 1){
                sum = a + b;
                a = b; b = sum;
                n--;
            }
            return sum
        }else{
            console.log("please enter number >= 0")
        }
    }

    const fibo = (n) =>{
        if(n <= 1){ return n; }
        var a=0, b=1, sum=0;
        while(n > 1) {
            sum = a + b;
            a = b; b =sum;
            n--;
        }
        return sum;
    }

    console.log(fibo(3))
    console.log(fibonacciMemo(3))

    /**Tests */
    // assert.strictEqual( fibonacci(3), 2 )
    // assert.strictEqual( fibonacci(6), 8 )
    // assert.strictEqual( fibonacciMemo(3), 2 )
    // assert.strictEqual( fibonacciMemo(6), 8 )
    // assert.strictEqual( fibonacciMemo(50), 12586269025 )
    // assert.strictEqual( fibonacciCalc(50), 12586269025 )

})()