(()=>{
    const assert = require("assert");
    /**
         * Move n-1 disks from start to middle using end
         * Move final disk from start to end
         * Move n-1 from middle to final using start
         * base condition n > 0
    */

   
    const hanoiTowers = (n, start, end, middle)=>{

        if(parseInt(n) > 0){
            var moveCounter = 0;
        
            const moveDisk = (from, to) =>{
               moveCounter++;
               console.log(`Move Disk from ${from} to ${to}`)
           }
   
           const moveTower = (n, start, end, middle) =>{
               if(n > 0){
                   moveTower(n-1, start, middle, end);
                   moveDisk(start, end);
                   moveTower(n-1, middle, end, start)
               }
           }
   
           moveTower(n, start, end, middle);
           return moveCounter;
        }else{
            console.log("please provide number greater than 1")
        }
        
    }

    // 
    //     let count = hanoiTowers(3, "A", "B", "C")
    //     console.log(`done..${count} moves`)
    // }


   /**Tests */
   assert.strictEqual( hanoiTowers(3, "A", "C", "B"), (Math.pow(2, 3) - 1))
   assert.strictEqual( hanoiTowers(4, "A", "C", "B"), (Math.pow(2, 4) - 1))

})()