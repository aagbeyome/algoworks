(() => {
    const assert = require("assert");

    let swap1ct = 0;
    const bubblesort = (arr) => {
        if (!Array.isArray(arr)) {
            console.log("invalid input")
            return;
        }

        let done = false;

        while (done == false) {
            done = true;
            for (let i = 0; i < arr.length - 1; i++) {
                let v1 = arr[i]; let v2 = arr[i + 1];
                if (v2 < v1) {
                    swap1ct++;
                    done = false;
                    arr[i] = v2;
                    arr[i + 1] = v1;
                }
            }
        }

        return arr;
    }

    const doMergeSort = (arr) =>{
        if(!Array.isArray(arr) || arr.length == 0){
            return;
        }

        var sortedLHalf=[], sortedRHalf = [];

        function getHalfs(arr){
            var median= Math.floor(arr.length / 2)
            var lhalf = [],rhalf = [];
            for(var i=0; i < arr.length; i++){
                if(i < median){
                    lhalf.push(arr[i])
                }else{
                    rhalf.push(arr[i])
                }
            }
            return [ lhalf, rhalf ]
        }

        //actual sorting happens here
        function mergeSort(parr, sortedArr){
           
            if(parr.length == 1){
                //put item in place in asc order
                var nInd = -1
                for(var i=0; i < sortedArr.length; i++){
                    if(sortedArr[i] >= parr[0]){
                        nInd = i;
                        break;
                    }
                }

                if(nInd > -1){
                    sortedArr.splice(nInd, 0, parr[0])
                }else{
                    sortedArr.push(parr[0])
                }
                
            }else if(parr.length > 1){
                var items = getHalfs(parr)
                mergeSort(items[0], sortedArr)
                mergeSort(items[1], sortedArr)
            }
        }

        function merge(arr1, arr2){

            for(var i=0; i < arr2.length; i++){
                
                var ind = -1;
                for(var k=0; k < arr1.length; k++){
                    if(arr1[k] >= arr2[i]){
                        ind = k;
                        break;
                    }
                }

                if(ind > -1){
                    arr1.splice(ind, 0, arr2[i])
                }else{
                    arr1.push(arr2[i])
                }
            }

            return arr1;

        }

        var parts = getHalfs(arr);
        
       mergeSort(parts[0], sortedLHalf)
       mergeSort(parts[1], sortedRHalf)
       return merge( sortedLHalf, sortedRHalf )
        
    }

    const quickSort = (arr) =>{
        if(!Array.isArray(arr) || arr.length == 0){
            return;
        }

        function swap(arr, i, j){
            var temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
        }

        //actual sorting happens here, we only arrange items around pivot but eventaully items will be sorted asc
        function partition(arr, low,  high){
            //pick a pivot, re-arrange arr items <= pivot => 
             //last element
            let pivot = arr[high];
          
            var i = low - 1;

            for(var j=low; j <= high-1; j++){
                if(arr[j] < pivot){
                    i++;
                    swap(arr, i, j);
                }
            }
            swap(arr, i + 1, high);
            return (i + 1);

        }

        function doSort(arr, low, high){
            if(low < high){
                var pi = partition(arr, low, high);
                
                doSort(arr, low, pi-1);
                doSort(arr, pi+1, high)
            }
        }

        doSort(arr, 0, arr.length-1);
        console.log(arr)
    }


    /* Bubble sort */
    // let arr = [9, 4, 6, 3, 2, 5, 7, 1, 6];
    // let vals = bubblesort(arr);
    // console.log(vals)

    var input = [9, 4, 6, 3, 2, 5, 7, 1, 6];
    //var input2 = [12,4,-1, 20, 11, 8, 0]
    //assert.deepStrictEqual(bubblesort(input), [1, 2, 3, 4, 5, 6, 6, 7, 9] )
    //assert.deepStrictEqual(doMergeSort(input), [1, 2, 3, 4, 5, 6, 6, 7, 9] )

    /**Merge Sort */
    console.log( doMergeSort(input) )

    

})()