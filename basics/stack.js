
const assert = require("assert");

var Stack = function () {
    this.items = [];

    this.push = function (item) {
        this.items.push(item);
    };

    this.pop = function () {
        return this.items.pop();
    };

    this.peek = function () {
        let len = this.size();
        var item;
        if (len > 0) {
            item = this.items[len - 1];
        }
        return item;
    };

    this.size = function () {
        return this.items.length;
    };
};

/** Tests
var myStack = new Stack();
myStack.push("a");
myStack.push("b");
myStack.push("c");

assert.strictEqual(myStack.size(), 3);
assert.strictEqual(myStack.peek(), "c");
assert.strictEqual(myStack.pop(), "c");
assert.strictEqual(myStack.size(), 2);
*/

module.exports = Stack;
