(() => {
    const assert = require("assert");

    class Node {

        constructor(value, next) {
            this.value = value;
            this.next = next || null;
        }
    }

    //the linked list only needs to know about the head as that will allow discovery of other nodes in link
    //if the head's next is null then thats the last node
    //the key here is to have a lastNode that will always point to the last added object

    //unordered linked list
    class LinkedList {

        constructor() {
            this.head = null;  //head node
        };

        /**
        * H:3,n -> 1:n -> 2:n -> null      
        */
        sort() {
            //bubble sort
            let notsorted = true

            while (notsorted) {
                notsorted = false
                let current = this.head, previous = null;

                const swapPair = (previous, current, next) => {
                    //detach node from link by creating new node pointing to what next is point to
                    let newCurNode = new Node(current.value, next.next)

                    //detach next node and attach new node to it i.e. newNext -> newCurNode
                    let newNext = new Node(next.value, newCurNode)

                    //attach to previous, if null attach to head
                    if (previous == null) {
                        this.head = newNext
                    } else {
                        previous.next = newNext
                    }
                }

                //traverse list
                while (current !== null) {
                    if (current.next == null) { break; }

                    if (current.value > current.next.value) {
                        notsorted = true
                        swapPair(previous, current, current.next)
                    }

                    previous = current
                    current = current.next
                }
            }
        };

        //this method essentially is just adding references of objects to objects,
        //its important for the nodes to be created outside of this function so they don't go out of scope
        //we add to beg
        addStart(node) {
            if (this.head == null) {
                this.head = node;   //first node
            } else {
                node.next = this.head;
                this.head = node;
            }
        };

        addEnd(node) {
            if (this.head == null) {
                this.head = node;
            } else {
                let current = this.head;
                while (current.next !== null) {
                    current = current.next
                }
                current.next = node;
            }
        };

        countNodes() {
            var ct = 0;
            let current = this.head;
            while (current !== null) {
                ct++;
                current = current.next;
            }
            return ct;
        };

        search(item) {
            let current = this.head;
            while (current !== null) {
                if (current.value == item) {
                    return true;
                }
                current = current.next;
            }
            return false
        };

        remove(value) {
            let previous = null, current = this.head;

            //traverse list
            while (current !== null) {
                if (current.value == value) {
                    break;
                }
                previous = current;
                current = current.next;
            }

            if (previous == null) {
                current = previous
            } else {
                previous.next = current.next;
            }
        };

        reverse(head) {
            let node = head, previous, tmp;
          
            while (node) {
              // save next before we overwrite node.next!
              tmp = node.next;
          
              // reverse pointer
              node.next = previous;
          
              // step forward in the list
              previous = node;
              node = tmp;
            }
            
          
            return previous;
        };


        myreverse(){
            let current = this.head, prev, tmp;

            while(current){
                tmp = current.next;
                current.next = prev;

                prev = current;
                current = tmp;
            }

            return prev;
        }


    };




    /**Tests */
    var myList = new LinkedList();
    myList.addEnd(new Node(1));
    myList.addEnd(new Node(2));
    myList.addEnd(new Node(3));
    myList.addEnd(new Node(4));
    myList.addEnd(new Node(5));

    // console.info(JSON.stringify(myList), "\n\n");

   let res = myList.myreverse()
    console.log(JSON.stringify(res));
    console.info(JSON.stringify(myList), "\n\n");

    /*
     assert.strictEqual(myList.countNodes(), 3)
     assert.strictEqual(myList.search(10), false)
     assert.strictEqual(myList.search(3), true)
     myList.remove(2)
     assert.strictEqual(myList.countNodes(), 2)
     assert.strictEqual(myList.search(2), false)
     */

    module.exports = { Node, LinkedList };
})()

