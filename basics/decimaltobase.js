(() => {
    const assert = require("assert");
    const Stack = require("./stack.js");

    const decimalToBase = (num, base) => {
        let res = "";
        let DIGITS = '0123456789abcdef'

        if (!isNaN(num)) {
            let binaryStack = new Stack();
            let divisor = num;

            if (num == 0) { binaryStack.push(num) }

            while (divisor > 0) {
                binaryStack.push(divisor % base)
                divisor = Math.floor(divisor / base);
            }

            while (binaryStack.size() > 0) {
                res = res + String(DIGITS[binaryStack.pop()])
            }
        }

        return res;
    }

    assert.strictEqual(decimalToBase(42, 2), "101010")


})()