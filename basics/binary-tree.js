(() => {
    const assert = require("assert");
   
    //left is < value
    // right >= value

    function TreeNode(value) {
        this.left;
        this.right;
        this.value = value;
    };

    function generateBinaryTree(arr) {
        if (!Array.isArray(arr) || arr.length == 0) {
            console.log("invalid arr")
            return;
        }

        let fval = arr.splice(0, 1)
        var root = new Tree(fval[0]);

        function addToNode(node) {
            if (arr.length > 0) {
                var lval = arr.splice(0, 1)
                var rval = arr.splice(0, 1)

                if (rval.length > 0) {
                    var llval = lval, rrval = rval;
                    if (lval > rval) {
                        llval = rval;
                        rrval = lval
                    }

                    node.left = new TreeNode(rrval[0]);
                    node.right = new TreeNode(llval[0]);

                    addToNode(node.left)
                    addToNode(node.right)

                } else {
                    node.left = new TreeNode(lval[0]);
                    addToNode(node.left)
                }
            }
        }

        addToNode(root)

        return root;
    }

    function generateBalancedBinarySearchTree(arr) {
        //every node has max 2 child nodes
        //every node has left node with lesser or equal key and righ node with greater or equal key
        //there are multiple ways to arrive at a binary pr sorted search tree, 
        //unless otherwise stated there is no requirement for tree to be balanced

        //algorithm
        //first sort nodes in asc order
        //take out the median node, split into 2 sub arrays and pick out the 2 medians from each sub-array to be left and right nodes
        //repeat til single nodes and add to left to right

        if (!Array.isArray(arr) || arr.length < 3) {
            console.log("invalid arr")
            return;
        }

        //sort arr, anyhow using bubblesort here
        var notsorted = true
        while (notsorted) {

            notsorted = false

            for (var i = 0; i < arr.length; i++) {
                var t1 = arr[i]; var t2 = arr[i + 1];
                if (t1 > t2) {
                    notsorted = true;
                    arr[i] = t2; arr[i + 1] = t1;
                    break;
                }

            }
        }

        //removes and returns the median of the array
        function getMedian(arr) {
            var indx = Math.floor(arr.length / 2);
            return arr[indx];
        }

        //splits array into 2 halfs, returns 3 items, left half, right half and [left median, median, righ median]
        function getHalfs(arr, node) {
            var left, right, leftSubArray = [], rightSubArray = [];

            if (arr.length > 2) {
                var medianIndex = Math.floor(arr.length / 2);


                for (var i = 0; i < arr.length; i++) {
                    if (i < medianIndex) {
                        leftSubArray.push(arr[i])
                    }
                    if (i > medianIndex) {
                        rightSubArray.push(arr[i])
                    }
                }


                left = getMedian(leftSubArray)
                right = getMedian(rightSubArray)

                node.left = new TreeNode(left)
                node.right = new TreeNode(right)

                if (leftSubArray.length > 0) {
                    getHalfs(leftSubArray, node.left)
                }

                if (leftSubArray.length > 0) {
                    getHalfs(rightSubArray, node.right)
                }

            } else {
                console.log(arr)
                var llval = arr.splice(0, 1)[0] || null;
                var rrval = arr.splice(0, 1)[0] || null
                node.left = (llval == null || llval == undefined) ? null : llval
                node.right = (rrval == null || rrval == undefined) ? null : rrval
            }

        }


        var medianIndex = Math.floor(arr.length / 2);
        var medianVal = arr[medianIndex]
        var root = new TreeNode(medianVal)

        getHalfs(arr, root)

        return root;
    }

    function traverseTree(node, srcVal) {
        //list all node values

        /*
        var current, found = false; var nodes = [];
        nodes.push(node.left, node.right)

        while(nodes.length > 0){
            current = nodes.shift()
            if(current){
                if(current.value == srcVal){
                    found = true;
                    break;
                }
    
                nodes.push(current.left, current.right)
            }
        }

        return found;
        */

        
       var ct=0;
        var chkNode = function(node, srcVal){
            if (node) {
              
                if(node.value == srcVal){
                    ct++;
                }
                chkNode(node.left, srcVal);
                chkNode(node.right, srcVal);
            }
        }
        
        chkNode(node, srcVal)
        return ct;
        
    }

    function insertNode(root, searchVal) {

        function findNode(node, searchVal) {
            if (!node) { return; }

            var refNode = node.left, opt = "left"

            if (node.value < searchVal) {
                opt = "right"
                refNode = node.right
            }

            var temp = refNode;
            var newNode = new TreeNode(searchVal)

            if (refNode) {
                if (refNode.value > searchVal) {
                    //traverse down tree
                    findNode(refNode, searchVal)
                } else {

                    //point on insertion
                    refNode = newNode
                    newNode.left = temp;
                    if (opt == "left") {
                        node.left = newNode
                    } else {
                        node.right = newNode
                    }
                }
            } else {
                //leaf node
                if (opt == "left") {
                    node.left = newNode
                } else {
                    node.right = newNode
                }
            }

        }

        findNode(root, searchVal)

    }

    function getMaxHeight2(node){
        function isnull(node){
            return (node == null || node == undefined)
        }

        if(node){
            if(isnull(node.left) && isnull(node.right)){
                return 0;
            }

            var maxLheight = getMaxHeight2(node.left);
            var maxRheight = getMaxHeight2(node.right);

            if(maxLheight > maxRheight){
                return maxLheight + 1
            }else{
                return maxRheight + 1
            }
        }
    }

    //my method
    function getMaxHeight(node){
        var ct = 0;

        function isnull(node){
            return (node == null || node == undefined)
        }

        if(isnull(node)){ return ct; }

        if( isnull(node.left) && isnull(node.right)){
            return ct; //empty node
        }

         //we traverse depth first
        function traverse(node){
           
           if(node){
                if(isnull(node.left) && isnull(node.right)){
                    return;
                }
               ct++;
               traverse(node.left);
               traverse(node.right)
           }
            
        }

        traverse(node);
        return ct;
    }

    //var arr = [1,2,3,4,7]
    //var tree = generateBalancedBinarySearchTree(arr);

    var tree = {
        "value": 4,
        "left": {
            "value": 2,
            "left": {
                "value" : 5,
                 "left" : 4,
                 "right" : {
                     "value" : 4,
                     "left" : 3,
                     "right" : 2
                 }
            },
            "right": 3
        },
        "right": {
            "value": 7
        }
    }

    var root1 = new TreeNode(8);
root1.left = new TreeNode(3);
root1.right = new TreeNode(10);
root1.left.left = new TreeNode(1);
root1.left.right = new TreeNode(6);
root1.left.right.left = new TreeNode(4);
root1.left.right.right = new TreeNode(7);
root1.right.right = new TreeNode(14);
root1.right.right.left = new TreeNode(13);

   // console.log(getMaxHeight(root1))
    //console.log(getMaxHeight2(root1))

    //console.log( traverseTree(tree,7) )
   // console.log(JSON.stringify(root1, null, "\t"))

})()