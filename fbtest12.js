// Add any extra import statements you may need here


// Add any helper functions you may need here
//123 => 3! = 123, 132, 213, 231, 312, 321
/*
    3(6)
2(10)   1(5)
    4(8)
*/

function generatePerms(str){
    if(str.length < 2){ return str; }
    var perms = [], char="", remChars="", tempArr = [];
    
    for(var i=0; i < str.length; i++){
      char = str[i];
      remChars = str.slice(0,i) + str.slice(i+1, str.length);
      tempArr = generatePerms(remChars)
      for(var ky in tempArr){
        perms.push( char + tempArr[ky] )
      }
    }

    return perms;
  }
  
  function calcAwkwardness(seatingArr, heights){
    if(!Array.isArray(seatingArr)){ return; }
    var vals = [], val=0, aId=0, bId=0;
    
    for(var i=0; i < seatingArr.length; i++){
      aId = i; bId = (i+1 == seatingArr.length) ? 0 : i+1;
      val = Math.abs( heights[ seatingArr[aId]] - heights[seatingArr[bId]] )
      vals.push( val )
    }
    
    vals.sort();
    
    return vals[vals.length-1]
  }
  
  function minOverallAwkwardness(arr) {
    // Write your code here
    
    var questIds = [], heights = {}, seatingArrangements = [], awkwardVals = []
    
    for(var i=0; i < arr.length; i++){
      heights[i+1] = arr[i]
      questIds.push(i+1)
    }

   seatingArrangements = generatePerms( questIds.join("") )

    
    for(var i=0; i < seatingArrangements.length; i++){
      awkwardVals.push( calcAwkwardness(seatingArrangements[i].split(""), heights) )
    }
    
    awkwardVals.sort()

    return awkwardVals[awkwardVals.length-1]
   
    
  }
  
  
  
  
  
  
  
  
  
  
  
  // These are the tests we use to determine if the solution is correct.
  // You can add your own at the bottom, but they are otherwise not editable!
  function printInteger(n) {
    var out = '[' + n + ']';
    return out;
  }
  
  var test_case_number = 1;
  
  function check(expected, output) {
    var result = (expected == output);
    var rightTick = "\u2713";
    var wrongTick = "\u2717";
    if (result) {
      var out = rightTick + ' Test #' + test_case_number;
      console.log(out);
    }
    else {
      var out = '';
      out += wrongTick + ' Test #' + test_case_number + ': Expected ';
      out += printInteger(expected);
      out += ' Your output: ';
      out += printInteger(output);
      console.log(out);
    }
    test_case_number++;
  }
  
  var arr_1 = [5, 10, 6, 8];
  var expected_1 = 4;
  var output_1 = minOverallAwkwardness(arr_1);
  check(expected_1, output_1);
  
  /*
  var arr_2 = [1, 2, 5, 3, 7];
  var expected_2 = 4;
  var output_2 = minOverallAwkwardness(arr_2);
  check(expected_2, output_2);
  */
  // Add your own test cases here
  