// Add any extra import statements you may need here


class Node {
    constructor(x) {
      this.data = x;
      this.next = null;
    }
  }
  
function clone(obj){
    return JSON.parse( JSON.stringify(obj) )
}

  function addNode(root, node){
    var current = root; var notdone = true
    while(current){
        if(current.next == null){
            current.next = node;
            break;
        }
        current = current.next
    }

    return JSON.parse(JSON.stringify(current))
  }
  // Add any helper functions you may need here
  // 1 -> 2 -> 8 -> 9 ==> 1 -> 8 -> 2 -> 9
  
  function reverse(head) {
    // Write your code here
    if(!head){ return; }

    var pointers = [];
    var evens = []; var nodeCt=0;

    var current = head;
    while(current){

        pointers.push(current);

        //even nodes
        if( current.data % 2 == 0 ){
            evens.push(nodeCt)
        }
            
        current = current.next;
        nodeCt++;
    }
    
    //[0,1,2,7,8]
    var evenGroups= [], evenSubGroup = [] 
    for(var i=0; i < evens.length; i++){
        if((evens[i+1] - evens[i]) == 1){
            evenSubGroup.push(evens[i])
        }else{
            evenSubGroup.push(evens[i])
            evenGroups.push(evenSubGroup)
            evenSubGroup = []
        }
    }

    //evenGroups = [ [ 2, 1, 0 ], [ 8, 7 ] ]
   //re-arrange pointers
   var group, revGroup, destInd, srcInd;
   for(var i=0; i < evenGroups.length; i++){
       group = evenGroups[i]; //[0,1,2]
       revGroup = clone(group);
       revGroup.reverse() //[2,1,0]
        var temps = []
       for(var k=0; k < revGroup.length; k++){
         destInd = revGroup[k];
         temps.push( pointers[destInd] ) 
       }

       for(var y=0; y < temps.length; y++){
        pointers.splice(group[y], 1, temps[y])
       }

      
   }

   //setup references
   for(var i=0; i < pointers.length; i++){
       pointers[i].next = null;
       var obj = JSON.parse(JSON.stringify( pointers[i] ));
       if(obj.next == null){
        pointers[i] = obj;
       }else{
        pointers[i] = obj.next
       }
   }

   var obj = pointers[0]; current = obj;
   for(var i=1; i < pointers.length; i++){
       var newNode = JSON.parse( JSON.stringify(pointers[i]))
       current.next = newNode;
       current = newNode
    }
  
    return obj;
  }
  
  
  
  function reverse2(head) {
    // Write your code here
    if(!head){ return; }

    var pointers = [];
    var evenPairs = []; var nodeCt=0;

    //var listObject = JSON.parse(JSON.stringify(head)), current, node, next, after;
    var current = head;
    while(current){
        pointers.push(current);

        //even nodes
        if( (current.data % 2 == 0) && current.next && (current.next.data % 2 == 0) ){
            

           node = current;  //2->8->9
           next = current.next; //8->9
           after = current.next.next; //9

           //delete children
           node.next = null; //node now 2
           next.next = null;  //next now 8

           //reconstruct and reverse
           node.next = after; // 2->9
           next.next = node; //8->2->9

          
           current = null;
           current = next;
           //console.info("before==>", JSON.stringify(current), "\n\n")
           //console.info( JSON.stringify(current),"\n\n")
           
        }
            
        current = current.next;
        console.info("current ==>",JSON.stringify(current), "\n\n head =>", JSON.stringify(head))
    }

   //console.log(JSON.stringify(head))

    return head;
  }
  
  

  
  // These are the tests we use to determine if the solution is correct.
  // You can add your own at the bottom, but they are otherwise not editable!
  var test_case_number = 1;
  
  function printLinkedList(head) {
    var out = '[';
    while (head != null) {
      out += head.data;
      head = head.next;
      if (head != null) {
        out += ' ';
      }
    }
    out += ']';
    return out;
  }
  
  function check(expectedHead, outputHead) {
    var result = true;
    var tempExpectedHead = expectedHead;
    var tempOutputHead = outputHead;
    while (expectedHead != null && outputHead != null) {
      result &= (expectedHead.data == outputHead.data);
      expectedHead = expectedHead.next;
      outputHead = outputHead.next;
    }
    if (!(expectedHead == null && outputHead == null)) result = false;
  
    var rightTick = "\u2713";
    var wrongTick = "\u2717";
    if (result) {
      var out = rightTick + ' Test #' + test_case_number;
      console.log(out);
    } else {
      var out = '';
      out += wrongTick + ' Test #' + test_case_number + ': Expected ';
      out += printLinkedList(tempExpectedHead);
      out += ' Your output: ';
      out += printLinkedList(tempOutputHead);
      console.log(out);
    }
    test_case_number++;
  }
  
  function createLinkedList(arr) {
    var head = null;
    var tempHead = head;
    for (var v of arr) {
      if (head == null) {
        head = new Node(v);
        tempHead = head;
      } else {
        head.next = new Node(v);
        head = head.next;
      }
    }
    return tempHead;
  }
  
  
  var head_1 = createLinkedList([1, 2, 8, 9, 12, 16]);
  var expected_1 = createLinkedList([1, 8, 2, 9, 16, 12]);
  var output_1 = reverse(head_1);
  check(expected_1, output_1);
  
  
  var head_2 = createLinkedList([2, 18, 24, 3, 5, 7, 9, 6, 12]);
  var expected_2 = createLinkedList([24, 18, 2, 3, 5, 7, 9, 12, 6]);
  var output_2 = reverse(head_2);
  check(expected_2, output_2);
  
  
  
  // Add your own test cases here
  