// Add any extra import statements you may need here

// Add any helper functions you may need here
function YearBook(id, signature) {
    this.id = id;
    this.signatures = signature || 0;
}

function Student(id, indx) {
    this.id = id;
    this.arrayIndex = indx
    this.currentYrBook = [new YearBook(id, 1)];

    this.receiveYearBook = function (yrBook, arr, outArr, arrIndx) {
        if (yrBook.id == this.id) {
             //out of game
            this.currentYrBook.push(JSON.parse(JSON.stringify(yrBook)));
            outArr.push(arr.splice(arrIndx, 1)[0])
            
        } else {
            yrBook.signatures = yrBook.signatures + 1;
            this.passYearBook(arr, outArr)
            this.currentYrBook.push(JSON.parse(JSON.stringify(yrBook)));
            this.passYearBook(arr, outArr)
        }
    }

    this.passYearBook = function (arr, outArr) {
        
        var yrBook = this.currentYrBook.pop();
        if (yrBook) {
            var recevingStudId = ((this.id - 1) < 0) ? 0 : (this.id - 1)
            if(arr.length > 0){
                recevingStudId = (recevingStudId >= arr.length) ? (arr.length - 1) : recevingStudId
            }
            arr[recevingStudId].receiveYearBook(JSON.parse(JSON.stringify(yrBook)), arr, outArr, recevingStudId)
        }
    }
}

function findSignatureCounts(arr) {
    // Write your code here
    // we can start from arr[0]
    // every student(n) passes to student as arr[n-1]
    // every student who get their book get out of process and we track how may signatures they had
    //each student will eventually get their book and will never have more than 1 book

    //map each arr item to a yearbook obj
    //while loop so long as there are year books to pass around
    //check anytime we passing of id mactches receiver, for exit

    var doneArr = [];

    if (!Array.isArray(arr)) { return; }

    for (var i = 0; i < arr.length; i++) {
        arr[i] = new Student(arr[i], i)
    }

   while (arr.length > 0) {
        //start passing of books
        for(var i=0; i < arr.length; i++){
            arr[i].passYearBook(arr, doneArr)
        }
   }

    //check signature counts from arr
    var sigCounts = []
    for (var i = 0; i < doneArr.length; i++) {
        sigCounts.push(0)
    }

    for (var i = 0; i < doneArr.length; i++) {
        var student = doneArr[i]
        if(student){
            var id = student.arrayIndex;
            var yearbook = student.currentYrBook.pop()
            if(yearbook){
                sigCounts[id] = yearbook.signatures;
            }
        }
       
    }

    return sigCounts;
}


// These are the tests we use to determine if the solution is correct.
// You can add your own at the bottom, but they are otherwise not editable!

function printintegerArray(array) {
    var size = array.length;
    var res = '';
    res += '[';
    var i = 0;
    for (i = 0; i < size; i++) {
        if (i !== 0) {
            res += ', ';
        }
        res += array[i];
    }
    res += ']';
    return res;
}

var test_case_number = 1;

function check(expected, output) {
    var expected_size = expected.length;
    var output_size = output.length;
    var result = true;
    if (expected_size != output_size) {
        result = false;
    }
    for (var i = 0; i < Math.min(expected_size, output_size); i++) {
        result &= (output[i] == expected[i]);
    }
    var rightTick = "\u2713";
    var wrongTick = "\u2717";
    if (result) {
        var out = rightTick + ' Test #' + test_case_number;
        console.log(out);
    }
    else {
        var out = '';
        out += wrongTick + ' Test #' + test_case_number + ': Expected ';
        out += printintegerArray(expected);
        out += ' Your output: ';
        out += printintegerArray(output);
        console.log(out);
    }
    test_case_number++;
}

var arr_1 = [2, 1];
var expected_1 = [2, 2];
var output_1 = findSignatureCounts(arr_1);
check(expected_1, output_1);

var arr_2 = [1, 2];
var expected_2 = [1, 1];
var output_2 = findSignatureCounts(arr_2);
check(expected_2, output_2);

  // Add your own test cases here
