// Add any extra import statements you may need here


// Add any helper functions you may need here
//[6,6,7,7,12] //19
function equalHalves(arr){
    var med=0, lhalf = [], uhalf = []
    var sum=0, medSum=0, splitSum = false, lhLower = true;
    
    //sum 10
     for(var i=0; i < arr.length; i++){
      sum = sum + arr[i]
     }
    
    //check for index where halfs sums are equal [1,2,2,5]
    for(var i=0; i < arr.length; i++){
      medSum = medSum + arr[i];
      if( (i < arr.length -1) && (sum/2 == medSum)){
        splitSum = true
        med = i;
        break;
      }
    }
    
    //check halves for less than rule
    if(med > 0){
      for(var i=0; i < arr.length; i++){
        if(i <= med){
          lhalf.push(arr[i])
        }else{
            uhalf.push(arr[i])
        }
      }
    
      for(var k=0; k < lhalf.length; k++){
         for(var y=0; y < uhalf.length; y++){
           if(lhalf[k] >= uhalf[y]){
             lhLower = false;
           }
         }
      }
    }

    return (splitSum && lhLower)
  }
  
  
  function balancedSplitExists(arr) {
    // Write your code here
    
    if(!Array.isArray(arr) || arr.length ==0){ return; }
    
    arr.sort();
    
    return equalHalves(arr)
    
    
  }
  
  
  
  
  
  
  
  
  
  
  
  // These are the tests we use to determine if the solution is correct.
  // You can add your own at the bottom, but they are otherwise not editable!
  function printString(str) {
    var out = '["' + str + '"]';
    return out;
  }
  
  var test_case_number = 1;
  
  function check(expected, output) {
    var result = (expected == output);
    var rightTick = "\u2713";
    var wrongTick = "\u2717";
    if (result) {
      var out = rightTick + ' Test #' + test_case_number;
      console.log(out);
    }
    else {
      var out = '';
      out += wrongTick + ' Test #' + test_case_number + ': Expected ';
      out += printString(expected);
      out += ' Your output: ';
      out += printString(output);
      console.log(out);
    }
    test_case_number++;
  }
  
  var arr_1 = [2, 1, 2, 5];
  var expected_1 = true;
  var output_1 = balancedSplitExists(arr_1); 
  check(expected_1, output_1); 
  
  /*
  var arr_2 = [3, 6, 3, 4, 4];
  var expected_2 = false;
  var output_2 = balancedSplitExists(arr_2); 
  check(expected_2, output_2); 
  */
  
  // Add your own test cases here
  