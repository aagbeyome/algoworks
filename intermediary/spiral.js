/**
 
X

0,0 0,1 0,2 0,3

1,0 1,1 1,2 1,3

2,0 2,1 2,2 2,3

3,0 3,1 3,2 3,3


Y
0  1  2  3   
- -----------
1  2  3  4   |0 
12 13 14 5   |1 X
11 16 15 6   |2
10  9  8 7   |3

 */

(() => {

    var assert = require("assert");

    //generate 2D matrix
    const genMatrix = (n) => {
        let matrix = [];
        for (var i = 0; i < n; i++) {
            let row = []
            for (var k = 0; k < n; k++) {
                row.push(null)
            }
            matrix.push(row)
        }
        return matrix;
    }

    //display the matrix
    const display = (vals) => {
        if (Array.isArray(vals)) {
            vals.forEach((val) => {
                console.log(val)
            })
        }
    }

    const spiral = (n) => {
        let end = (n - 1), val = 1;
        let XX = 0, YY = 0;
        let state = "ltr"
        let matrix = genMatrix(n);

        const getXYNext = (stateName) => {
            let sameState = stateName;
            let states = {
                "ltr": { "X": 0, "Y": 1, "next": "topDown" },
                "topDown": { "X": 1, "Y": 0, "next": "rtl" },
                "rtl": { "X": 0, "Y": -1, "next": "bottomUp" },
                "bottomUp": { "X": -1, "Y": 0, "next": "ltr" }
            }

            let state = states[stateName];
            let res = []

            if (state) {
                const { X, Y, next } = state

                if (X !== undefined && Y !== undefined && next !== undefined) {
                    let nextX = X + XX;
                    let nextY = Y + YY;

                    if ((nextX >= 0 && nextX <= end) && (nextY >= 0 && nextY <= end)) {
                        res = [nextX, nextY, sameState]
                        //check for collision
                        let chkval = matrix[nextX][nextY]
                        if (chkval !== null) {
                            if (X == 1) { nextX = XX; nextY = YY - 1 }
                            if (X == -1) { nextX = XX; nextY = YY + 1 }
                            if (Y == 1) { nextY = YY; nextX = XX + 1 }
                            if (Y == -1) { nextY = YY; nextX = XX - 1 }
                            res = [nextX, nextY, next]
                        }
                    } else {
                        if (nextX > end) { nextX = XX; nextY = YY - 1 }
                        if (nextY > end) { nextY = YY; nextX = XX + 1 }
                        if (nextY < 0) { nextY = YY; nextX = XX - 1 }
                        res = [nextX, nextY, next];
                    }
                }

            } else {
                console.log(`invalid state ${state}`)
            }

            return res;
        }

        if (Array.isArray(matrix)) {

            try {
                //insert first val and increment
                matrix[0][0] = 1; val++;

                while (val <= (n * n)) {
                    const [x, y, next] = getXYNext(state);
                    if (x !== undefined && y !== undefined && next !== undefined) {
                        state = next; XX = x; YY = y;
                        matrix[XX][YY] = val;
                    }
                    val++;
                }
            } catch (ex) {
                console.log(ex)
            }

            //done
            return matrix;
        }


    }


    //start program
    let n = 6;

    if (n < 1) {
        console.log("please provide input greater than 1")
        return;
    }
    let mymat = spiral(n);
    display(mymat)

    assert.deepStrictEqual(mymat[5], [16, 15, 14, 13, 12, 11], "incorrect output for entry 6")



})()