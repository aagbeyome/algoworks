

(function () {

    function Edge(from, to, weight, id) {
        this.from = from;
        this.to = to;
        this.weight = weight || 1;
        this.id = id || to.value;
    }

    function Vertex(value) {
        this.value = value;
        this.edges = {}; //edges have to be unique

        this.addEdge = function (e) {
            this.edges[e.id] = e
        };

        this.addVertex = function (v) {
            var toEdge = new Edge(this, v);
            this.addEdge(toEdge)
        };

        this.addWeightedEdgeVertex = function (v, w) {
            var edge = new Edge(this, v, w);
            this.addEdge(edge)
        };

        this.removeVertex = function (v) {
            //get nodes edges to find nodes its pointing to

            for (var ky in this.edges) {
                var edge = this.edges[ky]
                delete this.edges[ky]
                edge.to.removeEdge(v)
            }

        };

        this.removeEdge = function (v) {
            //scan through edges for edge to vertex v and delete it
            for (var k in this.edges) {
                var edge = this.edges[k]
                if (edge.id == v.value) {
                    delete this.edges[k]
                }
            }
        }
    }
 
    //depth first search traversal to find a path from start to end
    function dfs(start, end) {
        var res = []
        var visitedNodes = [];
        var allNodes = []

      
        if (!start || !end) { return; }

        
        var findPath = function (vertex, end, visitedNodes, res) {
            if (!vertex) { return; }

            if (vertex.value == end.value) {
                console.log(res) //we are at end node
            }

            visitedNodes.push(vertex.value);

            for (var ky in vertex.edges) {
                var vrtx = vertex.edges[ky].to;

                if (visitedNodes.indexOf(vrtx.value) < 0) {
                    res.push(vrtx.value)
                    findPath(vrtx, end, visitedNodes, res)
                    res.pop()
                }
            }

            visitedNodes.pop();
        };

        res.push(start.value)
        findPath(start, end, visitedNodes, res);
        
    }

    function depfs(start, end){
        if(!start || !end){ return; }
        var visitedNodes = [], paths = [], edges=[];

        if(start.value == end.value){
            paths.push(start.value); return; 
        }

        var findPath = function(vertex, end, visitedNodes, paths ){
            if(!vertex){ return; }

            if(vertex.value == end.value){
                return;
            }

            visitedNodes.push(vertex);

            for(var ky of vertex.edges.values()){
                if(visitedNodes.indexOf(ky.value) < 0){
                    paths.push(ky.value)
                    findPath(ky, end, visitedNodes, paths)
                    paths.pop()
                }
                
            }

            visitedNodes.pop();
        }

        paths.push(vertex.value)
        findPath(vertex, end, visitedNodes, paths)
    }


    /**       B - E - F
     *      / | \ |
     *    A - C - D
     */
    var E = new Vertex("E");
    var F = new Vertex("F");
    var A = new Vertex("A");
    var B = new Vertex("B");
    var C = new Vertex("C");
    var D = new Vertex("D");


    B.addVertex(A)
    B.addVertex(C)
    B.addVertex(D)
    B.addVertex(E)

    C.addVertex(A)
    C.addVertex(B)
    C.addVertex(D)

    A.addVertex(B)
    A.addVertex(C)

    D.addVertex(B)
    D.addVertex(C)
    D.addVertex(E)

    E.addVertex(B)
    E.addVertex(D)
    E.addVertex(F)

    F.addVertex(E)

    var mm = dfs(A, F);
    console.log(mm)
 

    //depthFirstSearch(A, F);
   // console.log(JSON.stringify(paths))



})()