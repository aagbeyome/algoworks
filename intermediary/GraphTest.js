(function () {

    /**       B - E - F
     *      / | \ | \
     *    A - C - D  H
     */

    function Edge(from, to, weight) {
        this.from = from;
        this.to = to;
        this.weight = weight || 1;
    }

    function Vertex(value) {
        this.value = value;
        this.edges = [];

        this.addEdge = function (v, w) {
            var edge = new Edge(this, v, w);
            this.edges.push(edge);
        }

    }

   
    //find all possible routes
    function depFSeach(start, end) {
      
        if (!start || !end) { return; }

        var visited = [];
        var paths = [];
        var allPaths = [];

        var traverse = function (vertex, end, visited, paths) {
                if (!vertex || !end) { return; }
           
                //check for end
                if (vertex.value == end.value) {
                    allPaths.push(JSON.parse(JSON.stringify(paths)));  //done
                }
    
                //track vertex
                visited.push(vertex.value);
               
                if (!Array.isArray(vertex.edges)) { return; }
    
                for (var i = 0; i < vertex.edges.length; i++) {
                    var nextVertex = vertex.edges[i].to;
    
                    //check that we've not already visited this node
                    if (visited.indexOf(nextVertex.value) == -1) {
                      
                        paths.push(nextVertex.value); //add new path
                        traverse(nextVertex, end, visited, paths);
                        paths.pop()
                    }
                }
    
                visited.pop()
        }

        paths.push(start.value);
        traverse(start, end, visited, paths);
        
        return allPaths;
    }

    //breadth first search
    function breadthFSearch(start, end){
        if(!start || !end){ return; }
       
        var queue = [],paths = [],visited = []

        queue.push(start);
        visited.push(start.value);

        while(queue.length > 0){
  
            var vertex = queue.shift()
            paths.push(vertex.value)

            if(vertex.value == end.value){
                break;
            }

            if(vertex){
                if(Array.isArray(vertex.edges)){
                    for(var i=0; i< vertex.edges.length; i++){
                        var nextVertex = vertex.edges[i].to;

                        if(visited.indexOf(nextVertex.value) < 0){
                            queue.push(nextVertex)
                            visited.push(nextVertex.value)
                        }
                    }
                }
            }
        }

        return paths;

    }

    function djikstra(start, end){

    }


    var A = new Vertex("A")
    var B = new Vertex("B")
    var C = new Vertex("C")
    var D = new Vertex("D")
    var E = new Vertex("E")
    var F = new Vertex("F")
    var H = new Vertex("H")

    A.addEdge(B, 1)
    A.addEdge(C, 3)

    B.addEdge(C, 2)
    B.addEdge(E, 5)
    B.addEdge(D, 3)

    E.addEdge(D, 1)
    E.addEdge(H, 4)
    E.addEdge(F, 2)

    /*
    A.addEdge(B)
    A.addEdge(C)

    B.addEdge(A)
    B.addEdge(C)
    B.addEdge(E)
    B.addEdge(D)

    E.addEdge(D)
    E.addEdge(B)
    E.addEdge(H)
    E.addEdge(F)

    C.addEdge(A)
    C.addEdge(B)
    C.addEdge(D)

    D.addEdge(E)
    D.addEdge(B)
    D.addEdge(C)

    F.addEdge(E)

    H.addEdge(E)
    */

    //var dd = depFSeach(C, H)
    //console.log(dd)

    var res = breadthFSearch(A, F)
    console.log(res)


})()