// Add any extra import statements you may need here


// Add any helper functions you may need here
//abcdcafag  , ccaab
function hasChars(str, ch){
    var strArr = str.split("")
    var chArr = ch.split("")
    var hasChars = false; var foundCt = 0;
    
    for(var i=0; i < chArr.length; i++){
        var ind = strArr.indexOf(chArr[i])
      if(ind > -1 ){
        strArr.splice(ind,1)
        foundCt++
      }
    }
    
    hasChars = (chArr.length == foundCt)
    
    return hasChars
    
  }
  
  function getSubstr(arr, st, charCt){
    var res = "";
    
    for(var i = st; i <= charCt; i++){
      if(i < arr.length){
        res = res + arr[i]
      }
    }
    
    return res;
  }
  
  function minLengthSubstring(s, t) {
    // Write your code here
    if(!s || !t || t== "" || s == ""){ return; }
    var ct = -1, srchArr = s.split(""), substr = "";
    
    //a,b,c,d
    //a ab abc abcd
    //b bc bcd
    for(var i=0; i < srchArr.length; i++){
      
      for(var k=i; k < srchArr.length; k++ ){
        substr = getSubstr(srchArr, i, k)
       
        if(hasChars(substr, t)){
           console.log(substr)
           ct = substr.length;
           return ct;
        }
        
      }
      
    }
    
    return ct;
  }
  
  
  
  // These are the tests we use to determine if the solution is correct.
  // You can add your own at the bottom, but they are otherwise not editable!
  function printInteger(n) {
    var out = '[' + n + ']';
    return out;
  }
  
  var test_case_number = 1;
  
  function check(expected, output) {
    var result = (expected == output);
    var rightTick = "\u2713";
    var wrongTick = "\u2717";
    if (result) {
        var out = rightTick + ' Test #' + test_case_number;
        console.log(out);
    }
    else {
        var out = '';
        out += wrongTick + ' Test #' + test_case_number + ': Expected ';
        out += printInteger(expected);
        out += ' Your output: ';
        out += printInteger(output);
        console.log(out);
    }
    test_case_number++;
  }
  

  
  var s_1 = "dcbefebce";
  var t_1 = "fd";
  var expected_1 = 5;
  var output_1 = minLengthSubstring(s_1, t_1);
  check(expected_1, output_1);

  
  
  var s_2 = "bfbeadbcbcbfeaaeefcddcccbbbfaaafdbebedddf";
  var t_2 = "cbccfafebccdccebdd";
  var expected_2 = -1;
  var output_2 = minLengthSubstring(s_2, t_2);
  check(expected_2, output_2);
  
  
  // Add your own test cases here
  