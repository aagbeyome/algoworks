(function(){

function Person(birthYr, deathYr){
    this.deathYr = deathYr;
    this.birthYr = birthYr;
}

function sort(arr){
    var sorted = false;

    while(sorted ==false){
        sorted = true;
        for(var i=0; i < arr.length-1; i++){
            if(arr[i] > arr[i+1]){
                sorted = false;
                var a = arr[i]; var b = arr[i+1];
                arr[i] = b; arr[i+1] = a;
            }
        }
    }
    return arr
}

//arr [Person]
function getHighestPopulation(arr){
    if(!Array.isArray(arr) || arr.length ==0 ){ return; }

    var yearlySummary = {}, results = {}
    //go through collection recording birth and death yrs and group by yr

    arr.forEach( function(person){
        var birthYr = person.birthYr;
        var deathYr = person.deathYr;

        if(yearlySummary[birthYr] == undefined) {
            yearlySummary[birthYr] = {"deathCount" :0, "birthCount" :0}
        }

        if(yearlySummary[deathYr] == undefined) {
            yearlySummary[deathYr] = {"deathCount" :0, "birthCount" :0}
        }

        yearlySummary[birthYr].birthCount = yearlySummary[birthYr].birthCount + 1;
        yearlySummary[deathYr].deathCount = yearlySummary[deathYr].deathCount + 1;

    })

    //process summary  
    /**
     * {
     *  2000 : { deathCt: 8, birthCt : 98 }, ...
     * }
     */

    var yrs = Object.keys(yearlySummary).sort();
    var totalPopulation = 0, maxYr = 0, maxCt=0;
    for(var i=0; i < yrs.length; i++){
        var curYr = yrs[i]
        var yrData = yearlySummary[curYr]
        var curTotal = totalPopulation + yrData.birthCount - yrData.deathCount
        results[yrs[i]] = curTotal;
        totalPopulation = curTotal;

        if(curTotal> maxCt){
            maxYr = curYr
            maxCt = curTotal
        }
       
    }

  return maxYr;
    
}


var person1 = new Person(1984, 2000)
var person2 = new Person(1989, 2000)
var person3 = new Person(2000, 2007)
var person4 = new Person(1983, 2000)
var person5 = new Person(1990, 2005)
var person6 = new Person(2000, 2005)

/**
 * 1983 - birth:1 , death:0, alive:0 => 1
 * 1984 - birth:1 , death:0, alive:0 => 2
 * 1989 - birth:1 , death:0, alive:0 => 3
 * 1990 - birth:1 , death:0, alive:0 => 4
 * 2000 - birth:2 , death:3, alive:0 => 3
 * 2005 - birth:0 , death:2, alive:0 => 1
*  2007 - birth:0 , death:1, alive:0 => 0
 */

var persons = [person1, person2, person3, person4, person5, person6]
var res = getHighestPopulation(persons);

})()