/**
 * 
 *     1        4
[1, 2, 5, 4, 3] = 

[5, 1, 3, 4, 2] | 1, 5, 3, 4, 2 | 1, 2, 4 , 3, 5

1, 2, 3, 4, 5


------------------
1) create a copy of the array in sorted asc order
2) loop throug the sorted arr,
  for each index in sorted array, locate its position in unsorted array and reverse substring
  from sorted array  element index to the unsorted arr elem index, this will get the 
  unsorted item in position
  track count of reversals
  check if strings are equal if yes exit, done

3) move to next item in sorted arr and repeat until unsorted arr is sorted in asc

 */
function arrSubStr(arr, st, end){
    var res = [];
    for(var i=st; i <= end; i++){
      res.push(arr[i]) 
    }
    return res.reverse();
  }
  
  function reverseArrSubstring(arr, st, reverseArr){
    for(var i=0; i < reverseArr.length; i++){
      arr[st + i] = reverseArr[i]
    }
    return arr
  }
  
  function minOperations(arr) {
    // Write your code here
    if(!Array.isArray(arr) || arr.length == 0){ return; }
    
    var reverseCt = 0;
    var sorted = JSON.parse(JSON.stringify(arr));
    sorted = sorted.sort();
    var substr = [], unsortedItemIndex =0;
    
    while(sorted.join("") !== arr.join("")){
      for(var i=0; i< sorted.length; i++){
        if(sorted[i] !== arr[i]){
           unsortedItemIndex = arr.indexOf(sorted[i])
          if(unsortedItemIndex > i){
            substr = arrSubStr(arr, i, unsortedItemIndex)
          }else{
            substr = arrSubStr(arr,unsortedItemIndex, i)
          }
          
           arr = reverseArrSubstring(arr, i, substr); reverseCt++;
        }
      }
    }

    return reverseCt;
    
  }
  
  
  
  minOperations([3, 1, 2])
  
  
  
  
  /*
  
  
  // These are the tests we use to determine if the solution is correct.
  // You can add your own at the bottom, but they are otherwise not editable!
  function printInteger(n) {
    var out = '[' + n + ']';
    return out;
  }
  
  var test_case_number = 1;
  
  function check(expected, output) {
    var result = (expected == output);
    var rightTick = "\u2713";
    var wrongTick = "\u2717";
    if (result) {
      var out = rightTick + ' Test #' + test_case_number;
      console.log(out);
    }
    else {
      var out = '';
      out += wrongTick + ' Test #' + test_case_number + ': Expected ';
      out += printInteger(expected);
      out += ' Your output: ';
      out += printInteger(output);
      console.log(out);
    }
    test_case_number++;
  }
  
  var n_1 = 5;
  var arr_1 = [1, 2, 5, 4, 3];
  var expected_1 = 1;
  var output_1 = minOperations(arr_1);
  check(expected_1, output_1);
  
  var n_2 = 3;
  var arr_2 = [3, 1, 2];
  var expected_2 = 2;
  var output_2 = minOperations(arr_2);
  check(expected_2, output_2);
  */
  // Add your own test cases here
  