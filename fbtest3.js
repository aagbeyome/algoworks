// Add any extra import statements you may need here


// Add any helper functions you may need here


function areTheyEqual(array_a, array_b){
    // Write your code here
    if(!Array.isArray(array_a) || !Array.isArray(array_b)){ return; }
    
    var arrAstr = array_a.join("");
    var arEqual = false;
    
    //we generate all the various substrings and reverse them and ccheck against arrStr
    
    function reverseArr(arr){
      arr = (typeof arr == "string") ? arr.split("") : arr
      var res="";
      
      for(var i=arr.length-1; i >=0; i--){
          res = res+""+arr[i];
      }
      return res;
    }
    
    function isEqual(astr, bstr){
      return (astr === bstr)
    }
    
    var stStr = "";
    var len = array_b.length, ct = 0;

    /**
var array_a_1 = [1, 2, 3, 4];
var array_b_1 = [1, 4, 3, 2];
     */

    function getSubstr(arr, st, end){
        var res = "";
        for(var i=st; i <= end; i++){
            res = res + String(arr[i])
        }
        return res;
    }

    for(var i=0; i < len; i++){
        var midStr="", endStr = "", finalStr = ""; 
        stStr = getSubstr(array_b, 0,i) 
        var subStrCt = (len-(i+1))  //3, 2, 1
        var endCt = 0;
        while(subStrCt > 0){
            midStr = reverseArr( getSubstr(array_b, i+1, (len-subStrCt))  )
            endCt = (len-subStrCt) + 1; 
            if(endCt <= len){
                endStr = getSubstr(array_b, endCt, (len-1));
            }
            finalStr = stStr+midStr+endStr
            if( finalStr === arrAstr ){
                return true;
                break;
            }
            subStrCt--;
        }
    }
    
    return arEqual;
  }
  

  
  // These are the tests we use to determine if the solution is correct.
  // You can add your own at the bottom, but they are otherwise not editable!
  function printString(str) {
    var out = '["' + str + '"]';
    return out;
  }
  
  var test_case_number = 1;
  
  function check(expected, output) {
    var result = (expected == output);
    var rightTick = "\u2713";
      var wrongTick = "\u2717";
    if (result) {
        var out = rightTick + ' Test #' + test_case_number;
        console.log(out);
    }
    else {
        var out = '';
        out += wrongTick + ' Test #' + test_case_number + ': Expected ';
        out += printString(expected);
        out += ' Your output: ';
        out += printString(output);
        console.log(out);
    }
    test_case_number++;
  }
  
  var array_a_1 = [1, 2, 3, 4];
  var array_b_1 = [1, 4, 3, 2];
  var expected_1 = true;
  var output_1 = areTheyEqual(array_a_1, array_b_1); 
  check(expected_1, output_1); 
  
  var array_a_2 = [1, 2, 3, 4];
  var array_b_2 = [1, 4, 3, 3];
  var expected_2 = false;
  var output_2 = areTheyEqual(array_a_2, array_b_2); 
  check(expected_2, output_2); 
  
  // Add your own test cases here
  