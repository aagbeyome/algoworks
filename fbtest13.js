var arr = [3,4,2]

/**
 * sub arrays
 * [3], [3,4], [3,4,2], [4,2], [2]
 */

function genSubArrayAvg(arr, st, end){
    var sub = [], sum=0, ct=0;
    for(var i=st; i <= end; i++){
        sub.push(arr[i])
        sum = sum + arr[i];
        ct++
    }
    return [sub, (sum/ct)];
}


var sum=0, subarrays = [], sub, avg;
for(var i=0; i < arr.length; i++){
    sum = sum + arr[i]
}


for(var i=0; i < arr.length; i++){
    for(var k=i+1; k < arr.length; k++){
        res = genSubArrayAvg(arr, i, k);
       if(res[1] > (sum/2)){
        subarrays.push(res[0])
       }
    }
}

console.log(subarrays);