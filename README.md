# AlgoWorks
```
 Algorithms and Data Structures

DATA STRUCTURES
Arrays, LinkedLists, Stacks, Queues, Trees, Graphs, HashTables

SEARCHING
Binary Search, BFS, DFS

SORTING
Merge Sort, Binary Sort, Quick Sort, Insertion Sort

CONCEPTS
Dynamic Programming (Recursion etc), Bit Manipulation

TIME COMPLEXITY

```
